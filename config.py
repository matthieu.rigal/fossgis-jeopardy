import categories

# TODO use a better format/structure?
class Config:
    music: dict[str, str] = {
        "thinking": "sounds/30_second_timer_with_jeopardy_thinking_music_6538041876554796686.mp3.opus",
    }

    sounds: dict[str, list[str]] = {
        "success": [
            "sounds/sfxr coin.wav.opus"
        ],
        "fail": [
            #"sounds/sat_1_geh_aufs_ganze_zonk_jingle_-8264698025220056228.mp3.opus",
            #"sounds/Legend Of Zelda Death Sound-3kxpcIUGgWY.m4a.opus",
            "sounds/yt1s.com - DöDöö.mp3.opus",
            #"sounds/yt1s.com - Nein Doch Ohh cut louder.mp3.opus",
        ],
        "task_fail": [
            "sounds/175409_1326576-lq.mp3.opus",
        ]
    }

    player_names_and_colors: list[list[str]] = [
        ["Player 1", "#96a43b"],
        ["Player 2", "#8d73ca"],
        ["Player 3", "#5ea476"],
        ["Player 4", "#c65b8a"],
        # ["Player 5", "#c76643"],
        # http://medialab.github.io/iwanthue/, should be CVD safe-ish
    ]

    port: int = 8000
    
    server: str = "localhost"

    categories: tuple[dict] = categories.spiel_20230317_categories
