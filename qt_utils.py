from PyQt5.QtWidgets import QWidget


def maximize_pointsize_in_widget(widget: QWidget, maximum_pointsize: int = 40):
    """Tries to grow the widget's font size to better fit the widget's size.

    For use e.g. with buttons or labels.

    Warning: Does not handle scaling *down* I think? At least the dialog's minimum
    size is growing too somehow...

    Args:
        widget: The widget for which the font size shall be maximized
        maximum_pointsize: Threshold for maximum point size, defaults to 80
    """
    # via https://forum.qt.io/topic/110696/dynamically-change-qpushbuttons-font-size
    # TODO move to utils.py
    font = widget.font()
    old_font_size = font.pointSizeF()

    if old_font_size >= maximum_pointsize:
        return

    h = widget.rect().height() - 8
    w = widget.rect().width() - 8
    step = 2
    text_rect = widget.fontMetrics().boundingRect(widget.text())
    while old_font_size <= maximum_pointsize and text_rect.height() < h and text_rect.width() < w:
        new_font_size = old_font_size + step
        font.setPointSizeF(new_font_size)
        widget.setFont(font)
        old_font_size = new_font_size
        text_rect = widget.fontMetrics().boundingRect(widget.text())

    # set result
    old_font_size -= step  # use the size before we didn't fit
    font.setPointSizeF(old_font_size)
    widget.setFont(font)
