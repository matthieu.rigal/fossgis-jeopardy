category = {
    "title": "Nage am Arm",
    "tasks": {
        100: {
            "task": "Monsteretappe",
            "solution": "OpenStreetMap",
            "reward": 100,
            "hint": "da spaziert man manchmal schon ziemlich viel in der Gegend herum für",
        },
        200: {
            "task": "Talelfe",
            "solution": "Leaflet",
            "reward": 200,
            "hint": "elfen sitzen gerne auf ästen, in der nähe von blättern",
        },
        300: {
            "task": "Sattelwal",
            "solution": "Weltatlas",
            "reward": 300,
        },
        400: {
            "task": "Reale Ponys",
            "solution": "OpenLayers",
            "reward": 400,
        },
        500: {
            "task": "Arg! Oh! Kapiert...",
            "solution": "Kartographie",
            "reward": 500,
            "hint": "puh… etwas mit ph",
        },
    }
}
