category = {
    "title": "Persönlichkeiten",
    "tasks": {
        100: {
            "task": "TODO",
            "solution": "TODO",
            "reward": 100,
        },
        200: {
            "task": "TODO",
            "solution": "TODO",
            "reward": 200,
        },
        300: {
            "task": "Diese Österreicherin schrieb u.a. Bücher zu Kartendesign mit QGIS und bändigt gerne sich bewegende Pandas.",
            "solution": "Anita Graser",
            "reward": 300,
        },
        400: {
            "task": """<em>"Everything is related to everything else, but near things are more related than distant things."</em>""",
            "solution": "Waldo Tobler",
            "reward": 400,
            "hint": "grundgesetz der topologie. klingt wie Schweizerische schokoladenleckerei, aber O(H)NE die richtige endung",
        },
        500: {
            "task": "Seine <code>lastools</code> revolutionierten die Arbeit mit Punktwolken und allem was dazugehört",
            "solution": "Martin Isenburg",
            "reward": 500,
            "hint": "Leider im 2021 von uns gegangen. Begeisterter Hühnerzüchter. Kampf mit Esri um das ideale Format für komprimierte Punktwolken, LAZ vs zLaz",
        },
    }
}
