# via lars roskoden, angepasst / hälfte von hannes
category = {
    "title": "Koordination",
    "tasks": {
        100: {
            "task": "90° 00′ S 000° 00′ E",
            "solution": "Der geographische Südpol",
            "reward": 100,
        },
        200: {
            "task": "00° 00′ S 000° 00′ E",
            "solution": '"Null Island"',
            "reward": 200,
        },
        300: {
            "task": "51° 28′ N 000° 00′ W",
            "solution": "Das Observatorium (der Garten) von Greenwich",
            "reward": 300,
        },
        400: {
            "task": "Dieses Kap liegt bei 55° 59′ S 067° 17′ W",
            "solution": "Kap Horn, der südlichste Punkte Südamerikas",
            "reward": 400,
        },
        500: {
            "task": "52° 25.866' N 13° 31.838' E",
            "solution": "⌖",
            "reward": 500,
        },
    }
}
