category = {
    "title": "LOGOsm",
    "tasks": {
        100: {
            "task": "image:osm logos/JOSM Logotype 2019 edited.png",
            "solution": "JOSM",
            "reward": 100,
            "hint": "sah früher anders aus, JOah..., Editor, Java",
        },
        200: {
            "task": "image:osm logos/Hot_logo edited.svg",
            "solution": "image:osm logos/Hot_logo.svg",
            "reward": 200,
            "hint": "BRENNpunkte werden gemappt. humanitäre hilfe.",
        },
        300: {
            "task": "image:osm logos/weeklyOSM_logo edited.svg",
            "solution": "image:osm logos/weeklyOSM_logo.svg",
            "reward": 300,
            "hint": "blog, wöchentlich, osm nachrichten, notizen",
        },
        400: {
            "task": "image:osm logos/streetcomplete.svg",
            "solution": "StreetComplete",
            "reward": 400,
        },
        500: {
            "task": "image:osm logos/potlatch3.png",
            "solution": "Potlatch",
            "reward": 500,
            "hint": """das tool ist nicht mehr da, wo es mal war. "BAN ...!" verrückter name, ein "rauschhaftes fest". flash basiert. ehemaliger editor auf osm.org"""
        },
    }
}
