category = {
    "title": "Numbers",
    "tasks": {
        100: {
            "task": "10000m²",
            "solution": "1 Hektar",
            "reward": 100,
        },
        200: {
            "task": "0,9996",
            "solution": "Maßstabsfaktor des Mittelmeridians bei der UTM-Projektion",
            "reward": 200,
        },
        300: {
            "task": "Der 400ste Teil eines Vollwinkels.",
            "solution": "1 Gon",
            "reward": 300,
        },
        400: {
            "task": "1/299 792 458 Sekunde",
            "solution": "1 Meter (den Licht in dieser Zeit im Vakuum zurücklegt)",
            "reward": 400,
        },
        500: {
            "task": "625 cm²",
            "solution": "Fläche von DIN A4",
            "reward": 500,
        },
    }
}
