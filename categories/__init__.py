from categories import (
    coordinates,
    osm_history,
    osm_logos,
    plz,
    points,
    projections,
    publikum_anagrams,
    publikum_logos,
    publikum_numbers,
    wat,
)

publikum_20230317_categories = (
    publikum_logos.category,
    publikum_numbers.category,
    publikum_anagrams.category,
)
spiel_20230317_categories = (
    projections.category,
    osm_logos.category,
    plz.category,
    osm_history.category,
    wat.category,
    points.category,
    coordinates.category,
)
