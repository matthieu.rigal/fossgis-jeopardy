category = {
    "title": "OHSOME",
    "tasks": {
        100: {
            "task": "video:osm history/hamburg.mp4",
            "solution": "Hamburg",
            "reward": 100,
        },
        200: {
            "task": "video:osm history/berlin.mp4",
            "solution": "Berlin",
            "reward": 200,
        },
        300: {
            "task": "video:osm history/dresden.mp4",
            "solution": "Dresden",
            "reward": 300,
        },
        400: {
            "task": "video:osm history/freiburg.mp4",
            "solution": "Freiburg",
            "reward": 400,
        },
        500: {
            "task": "video:osm history/helgoland.mp4",
            "solution": "Helgoland",
            "reward": 500,
        },
    }
}
