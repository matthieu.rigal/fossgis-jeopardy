category = {
    "title": "Projektionen",
    "tasks": {
        100: {
            "task": "Quadratisch, praktisch, aber stark verzerrend und ohne Darstellung der Pole dient diese Projektion als Basis für immer noch zu viele Webkarten.",
            "solution": "Pseudo / Web Mercator, EPSG:3857",
            "reward": 100,
            "hint": """Früher EPSG:900913(GOOGLE). Gilt nur zwischen ~85°N / S. Keine "richtige" Mercator-Projektion."""
        },
        200: {
            "task": "Dieser EPSG-Code steht für die UTM Zone 33N im Bezug auf das Europäische Terrestrische Referenzsystem 1989 (ETRS89)",
            "solution": "25833",
            "reward": 200,
        },
        300: {
            "task": "EPSG:4326",
            "solution": "WGS 84 / World Geodetic System 1984",
            "reward": 300,
            "hint": "GPS/GNSS, Koordinaten von -90 bis 90 und -180 bis 180, in Grad",
        },
        400: {
            "task": "Unter dieser Bezeichnung versteht man die Abbildung der geographischen Koordinaten (λ, φ) als kartesische Koordinaten (x, y).",
            "solution": "Plattkarte / Plate-Carrée / Rektangularprojektion",
            "reward": 400,
        },
        500: {
            "task": "Eine Vereinfachung der Erdgeometrie als Ikosaeder nutzt diese unterbrochene Projektion eines umtriebigen britischen Erfinders um Flächen, Formen und Winkel möglichst wenig zu verfälschen.",
            "solution": "Dymaxion-/Fuller-Projektion",
            "reward": 500,
            "hint": "Karte aus vielen Dreiecken. Entwickelte geodätische Kuppeln, Auto, Schlafrhythmus, ..."
        },
    }
}
