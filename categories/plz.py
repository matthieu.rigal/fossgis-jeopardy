# via marc tobias opencage, 2023, angepasst
# rolf der postzeitzahlen hand typ
category = {
    "title": "PLZ plz",
    "tasks": {
        100: {
            "task": "<code>boundary=postal_code, postal_code=12345</code>",
            "solution": "Mit diesem Tag werden Postleitzahlgebiete in OSM gemappt.",
            "reward": 100,
        },
        200: {
            "task": "1. Juli 1993",
            "solution": "An diesem Tag hat die Deutsche Post von 4- auf 5-stellige Postleitzahlen umgestellt.",
            "reward": 200,
        },
        300: {
            #"task": "01001",
            #"solution": "Die kleinste vergebene deutsche Postleitzahl (Stadtverwaltung Dresden)",
            "task": "11011",
            "solution": "Der Deutsche Bundestag hat diese Postleitzahl.",
            "reward": 300,
        },
        400: {
            "task": "Diese deutsche Exklave in der Schweiz hat sowohl in Deutschland als auch in der Schweiz eine Postleitzahl",
            "solution": "image:800px-Lage_von_büsingen_im_detail.svg.png",
            # source: https://commons.wikimedia.org/wiki/File:Lage_von_b%C3%BCsingen_im_detail.svg
            "reward": 400,
        },
        500: {
            "task": "Dieses winzige Nachbarland Frankreichs nutzt auch deren PLZ",
            "solution": "Monaco, 98xxx",
            "reward": 500,
        },
    }
}
