category = {
    "title": "Qoole Icons",
    "tasks": {
        100: {
            "task": "image:qgis gui/identify features.png",
            "solution": "Objekte abfragen",
            "reward": 100,
        },
        200: {
            "task": "image:qgis gui/toggle editing.png",
            "solution": "Bearbeitungsstatus umschalten",
            "reward": 200,
        },
        300: {
            "task": "image:qgis gui/open field calculator.png",
            "solution": "Feldrechner öffnen",
            "reward": 300,
        },
        400: {
            "task": "image:qgis gui/override.png",
            "solution": "Datendefinierte Übersteuerung",
            "reward": 400,
        },
        500: {
            "task": "image:qgis gui/open style manager.png",
            "solution": "Stilverwaltung",
            "reward": 500,
        },
    }
}
