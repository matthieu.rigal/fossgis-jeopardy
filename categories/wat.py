# https://codepen.io/cguillou/pen/ARpwgq
horror_css_1 = """
<style>
html, body, div {
   background-color: #880808;
}

.horror1 {
  font-size: 500%;
  color: #ccc;
  text-shadow: 0 0 5px rgba(0,0,0,0.5);

  position: absolute;
  left: 50%;
  top: 35%;

  animation: horror-animation 15s infinite;
}

@keyframes horror-animation {
	0% {text-shadow: 0 0 10px rgba(255,255,255,0.1); left: 31%; top: 36%;}
	8% {text-shadow: 0 0 10px rgba(255,255,255,0.2); left: 30%; top: 35%;}
  10% {text-shadow: 0 0 10px rgba(255,255,255,0.3); left: 29%; top: 34%;}
	12% {text-shadow: 0 0 10px rgba(255,255,255,0.4); left: 30%; top: 35%;}
  13% {text-shadow: 0 0 5px #212121; left: 30%}
  14% {text-shadow: 0 0 10px rgba(255,255,255,0.4); left: 30%;}
	18% {text-shadow: 0 0 10px rgba(255,255,255,0.5); left: 29%}
  25% {text-shadow: 0 0 10px rgba(255,255,255,0.6); left: 29%}
 	27% {text-shadow: 0 0 10px rgba(255,255,255,0.7); left: 30%}
	35% {text-shadow: 0 0 5px rgba(255,255,255,0.8); left: 30%}
  36% {text-shadow: 0 0 5px #212121; left: 30%}
  37% {text-shadow: 0 0 5px rgba(255,255,255,0.8); left: 30%}
  40% {text-shadow: 0 0 5px rgba(255,255,255,0.9); left: 31%}
	41% {text-shadow: 0 0 5px rgba(255,255,255,0.8); left: 31%}
	42% {text-shadow: 0 0 5px rgba(255,255,255,0.7); left: 31%}
	55% {text-shadow: 0 0 10px rgba(255,255,255,0.6); left: 30%}
  56% {text-shadow: 0 0 5px #212121; left: 30%}
  57% {text-shadow: 0 0 10px rgba(255,255,255,0.6); left: 32%}
  64% {text-shadow: 0 0 10px rgba(255,255,255,0.5); left: 30%}
	65% {text-shadow: 0 0 10px rgba(255,255,255,0.4); left: 30%}
	66% {text-shadow: 0 0 10px rgba(255,255,255,0.3); left: 29%}
  75% {text-shadow: 0 0 10px rgba(255,255,255,0.2); left: 29%}
  76% {text-shadow: 0 0 5px #212121; left: 30%}
  77% {text-shadow: 0 0 10px rgba(255,255,255,0.2); left: 29%}
 	80% {text-shadow: 0 0 5px rgba(255,255,255,0.1); left: 29%}
	85% {text-shadow: 0 0 5px rgba(255,255,255,0.2); left: 31%}
  86% {text-shadow: 0 0 5px #212121; left: 30%}
  87% {text-shadow: 0 0 5px rgba(255,255,255,0.2); left: 31%}
  90% {text-shadow: 0 0 5px rgba(255,255,255,0.3); left: 31%}
	95% {text-shadow: 0 0 10px rgba(255,255,255,0.4); left: 31%}
 	100% {text-shadow: 0 0 10px rgba(255,255,255,0.5); left: 30%}
}
</style>
"""

# https://cssdeck.com/labs/czvlzjop
horror_css_2 = """
<style>
html, body, div {
   background-color: black;
}

.horror2 {
  color: transparent;
  text-shadow: 0 0 5px rgba(0, 0, 0, 0.5);

  position: absolute;
  left: 50%;
  top: 35%;
  margin-left: -180px;
  animation: horror-animation 3s infinite;
}
@keyframes horror-animation {
  0% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.1);
    left: 41%;
    top: 36%;
  }
  5% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.2);
    left: 40%;
    top: 35%;
  }
  10% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.3);
    left: 39%;
    top: 34%;
  }
  15% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.4);
    left: 40%;
    top: 35%;
  }
  16% {
    text-shadow: 0 0 5px #212121;
    left: 40%;
  }
  17% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.4);
    left: 40%;
  }
  20% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.5);
    left: 39%;
  }
  25% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.6);
    left: 39%;
  }
  30% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.7);
    left: 40%;
  }
  35% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.8);
    left: 40%;
  }
  36% {
    text-shadow: 0 0 5px #212121;
    left: 40%;
  }
  37% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.8);
    left: 40%;
  }
  40% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.9);
    left: 41%;
  }
  45% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.8);
    left: 41%;
  }
  50% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.7);
    left: 41%;
  }
  55% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.6);
    left: 40%;
  }
  56% {
    text-shadow: 0 0 5px #212121;
    left: 40%;
  }
  57% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.6);
    left: 42%;
  }
  60% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.5);
    left: 40%;
  }
  65% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.4);
    left: 40%;
  }
  70% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.3);
    left: 39%;
  }
  75% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.2);
    left: 39%;
  }
  76% {
    text-shadow: 0 0 5px #212121;
    left: 40%;
  }
  77% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.2);
    left: 39%;
  }
  80% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.1);
    left: 39%;
  }
  85% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.2);
    left: 41%;
  }
  86% {
    text-shadow: 0 0 5px #212121;
    left: 40%;
  }
  87% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.2);
    left: 41%;
  }
  90% {
    text-shadow: 0 0 5px rgba(255, 0, 0, 0.3);
    left: 41%;
  }
  95% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.4);
    left: 41%;
  }
  100% {
    text-shadow: 0 0 10px rgba(255, 0, 0, 0.5);
    left: 40%;
  }
}
</style>
"""

category = {
    "title": "W.A.T.",
    "tasks": {
        100: {
            "task": "GIS",
            "solution": "<b>G</b>eographisches <b>I</b>nformations<b>s</b>ystem",
            "reward": 100,
        },
        200: {
            "task": "DTK",
            "solution": "<b>D</b>igitale <b>T</b>opographische <b>K</b>arte",
            "reward": 200,
        },
        300: {
            "task": "WFS-T",
            "solution": "<b>W</b>eb <b>F</b>eature <b>S</b>ervice mit Editierfunktion (<b>T</b>ransactional)",
            "reward": 300,
        },
        400: {
            "task": "COPC",
            "solution": "<b>C</b>loud <b>O</b>ptimized <b>P</b>oint <b>C</b>loud",
            "reward": 400,
        },
        500: {
            "task": f"""{horror_css_2}<span class="horror2" style="font-size:500%;"><em>ESRI</em></span>""",
            "solution": f"""{horror_css_2}<span class="horror2">E̵n̸v̴i̷r̵o̷n̴m̵e̷n̷t̵a̶l̵ ̶S̷y̸s̷t̸e̷m̶s̵ ̶R̴e̶s̶e̸a̶r̶c̷h̴ ̸I̴n̵s̷t̵i̴t̷u̶t̶e̵</span>""",
            "reward": 500,
        },
    }
}
