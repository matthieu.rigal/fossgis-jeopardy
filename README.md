# FOSSGIS-Jeopardy
A Jeopardy game in PyQt.

![screenshot](/uploads/f87401e336a54d7fac3310630ecc40b3/screenshot.png)

- Controlled via keyboard presses, e.g. via attached USB foot pedals as buzzers.
- QWebEngineView is used to display tasks because that gave me free video and audio and nice design options and stuff.
- Messy code in dire need of a clean refactor, nay!
- Lots of TODOs for improvement, hay!
- I had loads of fun building this, yay!
- Worked well for [FOSSGIS 2023](https://media.ccc.de/v/fossgis2023-23568-fossgis-jeopardy), yay!
- Code is licensed GPLv3. Please share your modifications :)
- The `categories` directory is not under a specific license as it contains third-party art and other things, use at your own discretion. You are welcome to re-use the tasks.
- The `sounds` directory is not under a specific license, because, uhm, you know. Freely licensed alternatives would be highly welcome.

# Installation

Create preferably a Python virtual environment by entering. Successfully tested with Python 3.11.
`python3 -m venv jeopardy`

Then install the requirements.
`pip install -r requirements.txt`

# Buzzers

It's recommended to use it with some programmable entry devices as buzzers for the teams. The USB foot switches (pedal),
from PCSensor, associated with [this configuration utility](https://github.com/rgerganov/footswitch) for Linux or OSX
are great for that! Follow the building instructions, but to support multiple devices, you may have to build it from
[the branch from this Pull Request](https://github.com/rgerganov/footswitch/pull/89).

You can check the status via
`footswitch -r`

You should then program the devices to give the devices the values of the keys "1,2,3,4". Using footswitch, this is done via
```
footswitch -d 0 -k 1
footswitch -d 1 -k 2
footswitch -d 2 -k 3
footswitch -d 3 -k 4
```

# Configuring the game

You'll want to change the player's names and the categories in the `config.py` file. For the categories, pay attention that these are defined as a dict inside a python file inside the `categories` folder. Note that this may change in the future.

# TODO
- add player name input dialog
- stop video playback when player wants to answer, resume afterwards if failed
- support local media files (currently run through a python server)
- gui for loading available pickled gamestate(s)
- update GUI according to game state in a more separated way
- track game state in game not buttons and labels ;D
- add tests?!
- add double jeopardy
- add bingo easteregg (1 player got one full row or column (or diagonal))
- hyphenation? hyphens: auto; and <html lang="de"> didn't work

# Thanks
- ray and sec for providing days of fun with their Hacker-Jeopardys at Chaos Communication Congresses.
- [Matthieu Rigal](https://gitlab.com/matthieu.rigal) for great contributions and changes to the code
