from functools import partial

from PyQt5.QtGui import QFont, QResizeEvent
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QPushButton, QSizePolicy

from game import Game
from qt_utils import maximize_pointsize_in_widget


class TasksGrid(QWidget):
    """A grid of buttons representing the categories and their tasks.

    When a button is clicked, the task is shown via the main window's functions.

    In return, the main window updates the button state after a task is finished (successful or not).
    """

    def __init__(self, game, parent):
        super().__init__(parent)
        self.game = game
        self.parent = parent
        self.clicked_button = None

        # references for fitting text sizes
        self.category_labels = []
        self.buttons = []

        self.update_from_game_state(self.game)

    def update_from_game_state(self, game: Game):
        """(Re-)Builds the labels and buttons according to the game state.

        Args:
            game: A Game state
        """

        tasks_grid_layout = QGridLayout()  # top row is category titles, other rows show tasks

        for column, category in enumerate(game.categories):
            category_title = QLabel(category["title"])
            category_title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)  # expand in both dimensions
            tasks_grid_layout.addWidget(category_title, 0, column)
            self.category_labels.append(category_title)

            for row, task in enumerate(category["tasks"].items(), start=1):
                reward = row * 100
                task = category["tasks"][reward]
                button = QPushButton(str(task["reward"]))
                button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)  # expand in both dimensions
                font = button.font()
                font.setPointSize(32)
                button.setFont(font)
                tasks_grid_layout.addWidget(button, row, column)

                # check if task was played already and build button accordingly (player color and clickable state)
                task_winner = task.get("winner")
                if task_winner is not None:  # player name or False -> has been played already
                    font = button.font()
                    font.setWeight(QFont.Light)
                    button.setFont(font)
                    button.setDisabled(True)
                    if task_winner is not False:
                        # TODO ugh use a better structure for players...
                        player_color = [p["color"] for p in game.players if p["name"] == task_winner][0]
                        button.setStyleSheet(f"background-color: {player_color};")

                button.clicked.connect(partial(self.open_task, task, button))
                self.buttons.append(button)

        self.setLayout(tasks_grid_layout)

    def open_task(self, task: dict, clicked_button: QPushButton) -> None:
        self.clicked_button = clicked_button
        self.parent.show_task(task)

    def disable_task_button(self) -> None:
        font = self.clicked_button.font()
        font.setWeight(QFont.Light)
        self.clicked_button.setFont(font)
        self.clicked_button.setDisabled(True)
        self.clicked_button = None

    def color_task_button(self, color: str) -> None:
        self.clicked_button.setStyleSheet(f"background-color: {color};")
        self.disable_task_button()

    def resizeEvent(self, event: QResizeEvent) -> None:
        """Tries to maximize the buttons' font size for better readability."""
        super().resizeEvent(event)
        for button in self.buttons:
            maximize_pointsize_in_widget(button)
        for label in self.category_labels:
            maximize_pointsize_in_widget(label, maximum_pointsize=20)
